#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import boto3
import hashlib
import json
import logging
import os
import re
import pytz
from math import floor
from datetime import datetime, timedelta
from pprint import pformat
from functools import partial

'''DOCUMENTATION

Resources tagged with:
mcf:scheduleDown - "0 19 * * *" (aka cron notation)
mcf:scheduleUp   - "0  7 * * *"
will be automagically put to sleep and woken up.
If the skiptag is present they will not be put to sleep:
mcf:skipSchedule - "anything"

We create scheduled cloudwatch actions on this lambda for specific resources for putting them to sleep and waking them up.

All times are in UTC, but if the mcf:scheduleTimeZone tag is set to something different all times will be converted from this zone to UTC.
(cloudwatch uses UTC so it'll be converted either way).
This is annoying since we have to modify cron notation, but we'll do it ;)

"Inventory mode"
- scan all ASG tags
- scan EC2 tags
- scan RDS instance/cluster tags
^-- triggered from cloudwatch event rule, by default runs once every hour

For every matched tag a (cloudwatch event/autoscaling) schedule is created change the target state (through this lambda).
When the downtime is activated the DESIRED and MIN and MAX count for an ASG will be set to 0, and a tag will be added with the original settings.
Next when the ASG is re-enabled these tags will be used to restore the MIN, MAX and DESIRED to the original values.
NOTE: It was considered to use the ASG schedules for putting to sleep and waking up, but ultimately not used due to cron notation and flexibility.
This lambda can be more flexible than a fixed schedule.

While down through this lambda the 'mcf:sandman:' prefixed tags will be present:
mcf:sandman:downtime         - time when sandman set this in downtime
mcf:sandman:previous:desired - old desired count of ASG
mcf:sandman:previous:min     - old minimum count of ASG
mcf:sandman:previous:max     - old maximum count of ASG

"Sandman mode"
- check if target ASG exists
- confirm that scheduleTag still exists and matches
- check if exception tag does not exist
- check wakeup trigger/tags exist, adjust/write them
- alter ASG min/max/desired

When deploying this lambda a created role ARN is passed in the 'SANDMAN_ROLE_ARN' environment variable which is used for lambda execution.

'''


class SandMan:
    def __init__(self):
        self.asg              = boto3.client('autoscaling')
        self.ec2              = boto3.client('ec2')
        self.rds              = boto3.client('rds')
        self.events           = boto3.client('events')
        self.blambda          = boto3.client('lambda')
        self.configure_tags()
        self.sandman_role_arn = self.getEnv("SANDMAN_ROLE_ARN", None)

    def configure_logger(self, loglevel=logging.INFO, logfile=None):
        logger = logging.getLogger()
        logger.setLevel(loglevel)
        format = "[%(filename)30s:%(funcName)20s():%(lineno)4s:%(levelname)-7s] %(message)s"
        if logfile and logfile is not None:
            logging.basicConfig(level=loglevel,
                                filename=logfile,
                                filemode='w',
                                format=format)
        else:
            logging.basicConfig(level=loglevel, format=format)

        logging.getLogger('boto3').setLevel(logging.WARNING)
        logging.getLogger('botocore').setLevel(logging.WARNING)

    def getEnv(self, name, default):
        if name in os.environ:
            return os.environ[name]
        else:
            return default

    def configure_tags(self):
        self.tags = {}
        tag_prefix = self.getEnv("TagPrefix", "mcf") + ':'
        self.tags['down']           = self.getEnv("ScaleDownTag", tag_prefix + "scheduleDown")
        self.tags['up']             = self.getEnv("ScaleUpTag", tag_prefix + "scheduleUp")
        self.tags['scheduled_down'] = self.getEnv("ScheduledDownTag", tag_prefix + "scheduledDown")
        self.tags['scheduled_up']   = self.getEnv("ScheduledUpTag", tag_prefix + "scheduledUp")
        self.tags['skip']           = self.getEnv("SkipTag", tag_prefix + "skipSchedule")
        self.tags['sandman']        = self.getEnv("SandmanTag", tag_prefix + "sandman:")
        self.tags['zone']           = self.getEnv("TimeZoneTag", tag_prefix + "scheduleTimeZone")
        # Sleep tags
        self.tags['sandman_min']    = self.getEnv("SandmanMin", self.tags['sandman'] + 'previous:min')
        self.tags['sandman_max']    = self.getEnv("SandmanMax", self.tags['sandman'] + 'previous:max')
        self.tags['sandman_des']    = self.getEnv("SandmanDes", self.tags['sandman'] + 'previous:des')
        logging.debug("Tags configured as follows: %s" % (pformat(self.tags)))

    def handle(self, event, context):
        logging.debug("Lambda received the event {} with context {}".format(pformat(event), pformat(context)))
        self.event     = event
        self.context = context
        # try:
        #    self.region        = event['region']
        #    self.detail        = event['detail']
        #    self.eventname = self.detail['eventName']
        #    self.arn             = self.detail['userIdentity']['arn']
        #    self.principal = detail['userIdentity']['principalId']
        #    self.userType    = detail['userIdentity']['type']
        #
        #    logging.info('principalId: {}'.format(principal))
        #    logging.info('region         : {}'.format(region))
        #    logging.info('eventName    : {}'.format(eventname))
        #    logging.info('detail         : {}'.format(detail))
        # except Exception as e:
        #    logging.error('Error parsing event {}: {}'.format(pformat(event), pformat(e)))
        #    return False

        # Check running mode
        mode = event.get('mode', 'inventory')
        if mode == 'inventory':
            return self.inventory()
        elif mode == 'sandman':
            return self.sandman()
        else:
            logging.error('Unknown execution mode {} - abort'.format(mode))
            return False
        return True

    def paginated_response(self, func, result_key, next_token=None):
        '''
        Borrowed from Ansible
        Returns expanded response for paginated operations.
        The 'result_key' is used to define the concatenated results that are combined from each paginated response.
        '''
        args = dict()
        if next_token:
            args['NextToken'] = next_token

        response   = func(**args)
        result     = response.get(result_key)
        next_token = response.get('NextToken')

        if not next_token:
            return result

        return result + self.paginated_response(func, result_key, next_token)

    def getScheduleRuleName(self, asg, function):
        # Returns schedule name based on asg/function/schedule.
        # Note that max length can be 64, so we truncate and hash ASG name for consistency
        # I.e. input "wdegeus-ecs-asg", "up" -> out "SandMan-wdegeus-ecs-asg-65e078988d94383c4d55f945a-up"
        #
        # Rule allows pattern: [\.\-_A-Za-z0-9]+.
        # Our sandman--- takes up 10 chars, reserve 4 for function, leave 50 for ASG plus hash, split it 32/18
        asghash    = (hashlib.md5(asg.encode("utf-8")).hexdigest())[0:18]
        myasg      = (re.sub('[^.-_0-9a-zA-Z-]+', '-', asg))[0:32]
        myfunction = re.sub('[^.-_0-9a-zA-Z-]+', '-', function)
        return "SandMan-{}-{}-{}".format(myasg, asghash, myfunction)

    def deleteSchedule(self, asg, function):
        logging.info("Removing SandMan {} schedule for asg {}".format(tag, asg))
        rulename = self.getScheduleRuleName(asg, tag)
        if rulename in self.rules:
            # First remove all targets, otherwise we can't remove the rule
            func = partial(self.events.list_targets_by_rule, **{'Rule': rulename})
            ids = self.paginated_response(func, 'Targets')
            try:
                res = self.events.remove_targets(Rule=rulename, Ids=ids)
            except Exception as e:
                logging.error("Error removing targets {} for rule {} for asg {} function {}: {}".format(pformat(ids), rulename, asg, function, pformat(e)))
                return False
            # Next remove the rule itself.
            try:
                res = self.events.delete_rule(Name=rulename)
            except Exception as e:
                logging.error("Error deleting rule {} for asg {} function {}: {}".format(rulename, asg, function, pformat(e)))
                return False
        else:
            logging.debug("Rule {} already deleted - not retrying that ;)".format(rulename))
        # Finally remove the tags
        try:
            self.asg.delete_tags(Tags=[{
                'ResourceId': asg,
                'ResourceType': 'auto-scaling-group',
                'Key': self.tags['scheduled_' + function]
            }])
        except Exception as e:
            logging.error("Error deleting tag {} for asg {} function {} after deleting rule: {}".format(self.tags['scheduled_' + function], asg, function, pformat(e)))
            return False
        logging.info("Deleted rule {}".format(rulearn))

    def createSchedule(self, asg, function, schedule, srczone=None):
        if srczone is not None:
            utcschedule = self.cronToUTC(schedule, srczone)
        else:
            utcschedule = schedule
        logging.info("Creating SandMan {} schedule for asg {} with schedule cron({}) modified to UTC as cron({})".format(function, asg, schedule, utcschedule))
        schedule = utcschedule
        # Creates or updates schedule rule for ASG
        rulename = self.getScheduleRuleName(asg, function)
        if len(schedule.split()) != 6:
            logging.error("ERROR: Schedule for asg {} function {} is set to cron({}), but this is invalid - we expect 6 parts but got {}! SKIPPING".format(asg, function, schedule, len(schedule.split())))
            return False
        # Get rid of excess whitespace, the schedules are very very picky.... a double space leads to an invalid expression :/
        schedule = " ".join(schedule.split())
        # Check old rules
        if rulename in self.rules:
            # See if the schedule changed
            logging.debug("Schedule for asg {} function {} is set to cron({}), comparing against old schedule...".format(asg, function, schedule))
            try:
                schedule_old = self.rules[rulename]['ScheduleExpression']
                if schedule_old == ("cron(" + schedule + ")"):
                    logging.debug("Old schedule {} matches new schedule {} - not altering rule.".format(schedule_old, schedule))
                    return True
                else:
                    logging.debug("Old schedule {} does not match new schedule cron({}) - updating rule!".format(schedule_old, schedule))
            except Exception as e:
                logging.error("Error comparing old vs new schedule for rule {} -- updating rule to be certain! Error was: {}".format(rulename, pformat(e)))
        try:
            res = self.events.put_rule(
                Name=rulename,
                ScheduleExpression="cron(" + schedule + ")",
                # EventPattern='string',
                State='ENABLED',
                Description='SandMan generated rule for ASG {} {}'.format(asg, function),
                # RoleArn=self.sandman_rule_arn
            )
        except Exception as e:
            logging.error("Error creating/updating rule {} for asg {} function {} with schedule cron({}): {}".format(rulename, asg, function, schedule, pformat(e)))
            return False
        rulearn = res['RuleArn']
        logging.info("Created rule {}".format(rulearn))

        # Add this lambda as schedule target (self.context.function_name / invoked_function_arn)
        logging.info("Adding this lambda {} as target for rule {}".format(self.context.invoked_function_arn, rulename))
        try:
            res = self.events.put_targets(
                Rule=rulename,
                Targets=[{
                    'Id': rulename,
                    'Arn': self.context.invoked_function_arn,
                    # "ValidationException) when calling the PutTargets operation: RoleArn is not supported for target arn:aws:(..):function:SandMan"
                    # 'RoleArn': self.sandman_role_arn,
                    # Input parameters for sandman mode
                    'Input': json.dumps({
                        'asg': asg,
                        'function': function,
                        'mode': 'sandman',
                    })
                }])
        except Exception as e:
            logging.error("Error creating/updating rule targets for rule {}: {}".format(rulename, pformat(e)))
            return False

        # Add permissions for event rule to invoke lambda:
        logging.info("Giving event permissions to invoke this lambda {} as target for rule {}".format(self.context.function_name, rulearn))
        try:
            self.blambda.add_permission(
                FunctionName=self.context.function_name,
                StatementId=rulename,
                Action='lambda:InvokeFunction',
                Principal='events.amazonaws.com',
                SourceArn=rulearn
            )
        except Exception as e:
            logging.warning("WARNING: failed to add event invoke permission for this lambda to rule arn {}: {} - ignoring this error, but if your lambda does not trigger this might be why!".format(rulearn, pformat(e)))

        # Write schedule rule arn to ASG tag
        asgtag = self.tags['scheduled_' + function]
        logging.debug("Writing scheduled rule arn {} to asg tag {}".format(rulearn, asgtag))
        try:
            res = self.asg.create_or_update_tags(
                Tags=[{
                    'ResourceId': asg,
                    'ResourceType': 'auto-scaling-group',
                    'Key': asgtag,
                    'Value': rulearn,
                    'PropagateAtLaunch': False
                }])
        except Exception as e:
            logging.error("Error creating/updating asg tags on asg {}: {}".format(asg, pformat(e)))
            return False

    def getAsgMinMaxDes(self, asgname):
        try:
            func = partial(self.asg.describe_auto_scaling_groups, **{'AutoScalingGroupNames': [asgname]})
            asgs = self.paginated_response(func, 'AutoScalingGroups')
            if len(asgs) != 1 or asgs[0]['AutoScalingGroupName'] != asgname:
                logging.error("ERROR: Could not describe ASG {}?!".format(asgname))
                return []
            res = asgs[0]
        except Exception as e:
            logging.error("ERROR: Error describing ASG {}: {}".format(asgname, pformat(e)))
            return []
        return [res['MinSize'], res['MaxSize'], res['DesiredCapacity']]

    def getAsgTags(self, asgname=None):
        # ASG - fetch all groups that have the up or down tag
        pager = self.asg.get_paginator('describe_tags')
        filters = [{
            'Name': 'key',
            'Values': [
                self.tags['up'],
                self.tags['scheduled_up'],
                self.tags['down'],
                self.tags['scheduled_down'],
                self.tags['sandman_min'],
                self.tags['sandman_max'],
                self.tags['sandman_des'],
                self.tags['zone'],
            ]
        }]
        if asgname is not None:
            filters.append({
                'Name': 'auto-scaling-group',
                'Values': [asgname],
            })
        response = pager.paginate(Filters=filters).build_full_result()
        asgs = {}
        for item in response['Tags']:
            asg = item['ResourceId']
            if asg not in asgs:
                asgs[asg] = {}
            asgs[asg][item['Key']] = item['Value']
        return asgs

    def cronToUTC(self, schedule, zone):
        ''' Converts a $zone schedule to a UTC variant, adjusting hours and minutes only(!) '''
        (sminutes, shours, sdays, smonths, sdows, syears) = schedule.split()
        dt = datetime.now()
        srctz = pytz.timezone(zone)
        offset_hours = floor(srctz.utcoffset(dt) / timedelta(hours=1))
        offset_mins = floor((srctz.utcoffset(dt) / timedelta(minutes=1)) % 60)
        logging.debug("[cronToUTC] Timezone to UTC offset hours[{}] minutes[{}]".format(offset_hours, offset_mins))
        rmins = []
        for m in sminutes.split(','):
            if type(m) == str and (m[0] == '*' or m[0] == '?'):
                # Don't need to change this
                rmins.append(m)
            else:
                res = int(m) - offset_mins
                if res < 0:
                    res += 60
                if res > 60:
                    res -= 60
                rmins.append(str(int(res)))
        rhours = []
        for h in shours.split(','):
            if type(h) == str and (h[0] == '*' or h[0] == '?'):
                # Don't need to change this
                rhours.append(h)
            else:
                res = int(h) - offset_hours
                if res < 0:
                    res += 24
                if res > 24:
                    res -= 24
                rhours.append(str(int(res)))
        return "{} {} {} {} {} {}".format(",".join(rmins), ",".join(rhours), sdays, smonths, sdows, syears)

    def inventory(self):
        logging.info("SandMan Inventory mode, scanning...")

        # ASG - fetch all groups that have the up or down tag
        asgs = self.getAsgTags()

        # Also grab all cloudwatch schedules for comparison, we prefix all rules with 'SandMan'
        func = partial(self.events.list_rules, **{'NamePrefix': 'SandMan'})
        event_rules = self.paginated_response(func, 'Rules')
        ruledict = {}
        for e in event_rules:
            func = partial(self.events.list_targets_by_rule, **{'Rule': e['Name']})
            e['targets'] = self.paginated_response(func, 'Targets')
            ruledict[e['Name']] = e
        self.rules = ruledict

        # Next, check if all groups with up/down tags have a scheduled_up and scheduled_down
        for asg in asgs:
            zone = None
            if self.tags['zone'] in asgs[asg]:
                zone = asgs[asg][self.tags['zone']]
            if self.tags['up'] in asgs[asg] or self.tags['down'] in asgs[asg]:
                # if not scheduled_up/down, create it, or if changed, update it
                if self.tags['up'] in asgs[asg]:
                    self.createSchedule(asg, 'up', asgs[asg][self.tags['up']], zone)
                if self.tags['down'] in asgs[asg]:
                    self.createSchedule(asg, 'down', asgs[asg][self.tags['down']], zone)
            else:
                # if it does have scheduled_up/down but no up/down anymore, remove it
                if self.tags['scheduled_down'] in asgs[asg]:
                    self.deleteSchedule(asg, 'down')
                if self.tags['scheduled_up'] in asgs[asg]:
                    self.deleteSchedule(asg, 'up')
        # TODO: EC2
        # TODO: RDS
        logging.info("Inventory complete, checked {} asgs...".format(len(asgs)))

    def sandman(self):
        # Figure out who we are putting to sleep or waking up
        asg = self.event.get('asg')
        rds = self.event.get('rds')
        ec2 = self.event.get('ec2')
        function = self.event.get('function')
        logging.info("Sandman invoked for function {} and (ASG {}/RDS {}/EC2 {})".format(function, asg, rds, ec2))
        if function not in self.tags:
            logging.error("ERROR: Function {} not present in our tags -- this should not happen! Tags known: ".format(function, pformat(tags)))
            return
        if asg is not None:
            # Called for ASG
            # Check that tags are still present (tags[function])
            tags = self.getAsgTags(asg)
            if asg not in tags or self.tags[function] not in tags[asg]:
                logging.warning("ASG that we were called for {} no longer seems to have the necessary tags! Skipping!".format(asg))
                return
            # Check if there is a skip tag present
            if self.tags['skip'] in tags[asg]:
                logging.warning("ASG that we were called for {} has skip tag {} on it - Skipping!".format(asg, self.tags['skip']))
                return
            # If function is down:
            if function == 'down':
                # Get current settings
                try:
                    asgmin, asgmax, asgdes = self.getAsgMinMaxDes(asg)
                except Exception as e:
                    logging.error("Error fetching current asg min/max/desired settings for asg {} - not sleeping!: {}".format(asg, pformat(e)))
                    return False
                # Write current settings to sandman tags if that makes sense
                if self.tags['sandman_min'] in tags[asg] or self.tags['sandman_max'] in tags[asg] or self.tags['sandman_des'] in tags[asg]:
                    logging.warning("ASG that we were called for {} (still?) has sandman sleep tags on it - Skipping!".format(asg))
                    return False
                if asgmax == 0:
                    logging.warning("Current max setting for asg {} is already set to 0 -> not sleeping!".format(asg))
                    return False
                logging.debug("Writing sandman tags min[{}] max[{}] desired[{}] to asg {}".format(asgmin, asgmax, asgdes, asg))
                try:
                    res = self.asg.create_or_update_tags(
                        Tags=[
                            {'ResourceId': asg, 'ResourceType': 'auto-scaling-group', 'Key': self.tags['sandman_min'], 'Value': str(asgmin), 'PropagateAtLaunch': False},
                            {'ResourceId': asg, 'ResourceType': 'auto-scaling-group', 'Key': self.tags['sandman_max'], 'Value': str(asgmax), 'PropagateAtLaunch': False},
                            {'ResourceId': asg, 'ResourceType': 'auto-scaling-group', 'Key': self.tags['sandman_des'], 'Value': str(asgdes), 'PropagateAtLaunch': False},
                        ]
                    )
                except Exception as e:
                    logging.error("Error creating/updating asg tags on asg {}: {}".format(asg, pformat(e)))
                    return False
                asgmin = 0
                asgmax = 0
                asgdes = 0
            # If function is up:
            elif function == 'up':
                # Check old settings from sandman tags
                try:
                    asgdes = int(tags[asg][self.tags['sandman_des']])
                    asgmin = int(tags[asg][self.tags['sandman_min']])
                    asgmax = int(tags[asg][self.tags['sandman_max']])
                except Exception as e:
                    logging.error("ERROR - SANDMAN TAGS FOR RESTORING THE ASG {} ARE MISSING!!! (Wtf - did you change tags?)!!! Have these tags: {}, need {}, {} and {}".format(asg, pformat(tags), self.tags['sandman_min'], self.tags['sandman_max'], self.tags['sandman_des']))
                    return
            else:
                logging.error("Function {} is not down or up -- what are you trying? ABORT.".format(function))
                return
            # Update ASG
            logging.info("ASG Mode, going to {} asg {} - setting min[{}] max[{}] desired[{}]".format(function, asg, asgmin, asgmax, asgdes))
            try:
                self.asg.update_auto_scaling_group(
                    AutoScalingGroupName=asg,
                    MinSize=asgmin,
                    MaxSize=asgmax,
                    DesiredCapacity=asgdes
                )
            except Exception as e:
                logging.warning("ERROR: Failed to update ASG {} to bring it {}: {}".format(asg, function, pformat(e)))
                return False
            # Delete sandman tags
            if function == 'up':
                logging.info("Deleting sandman tags not that ASG {} is up again...".format(asg))
                try:
                    self.asg.delete_tags(Tags=[
                        {'ResourceId': asg, 'ResourceType': 'auto-scaling-group', 'Key': self.tags['sandman_min']},
                        {'ResourceId': asg, 'ResourceType': 'auto-scaling-group', 'Key': self.tags['sandman_max']},
                        {'ResourceId': asg, 'ResourceType': 'auto-scaling-group', 'Key': self.tags['sandman_des']},
                    ])
                except Exception as e:
                    logging.error("ERROR: Failed to delete sandman tags after bringing up ASG {} - NEXT SLEEP WILL FAIL!: {}".format(asg, pformat(e)))
                    return False
        else:
            logging.error("NOT YET IMPLEMENTED!")
##############################
#    End of SandMan Class    #
##############################


def lambda_handler(event, context):
    loglevel         = os.environ['LogLevel'] if 'LogLevel' in os.environ else logging.INFO
    sm = SandMan()
    sm.configure_logger(loglevel)
    return sm.handle(event, context)


if __name__ == "__main__":
    # import argparse
    # parser = argparse.ArgumentParser(description='AWS Resource Tagger')
    # args = parser.parse_args()
    event = {
        "version": "0",
        "id": "01234567-0123-0123-0123-012345678901",
        "detail-type": "EBS Snapshot Notification",
        "source": "aws.ec2",
        "account": "012345678901",
        "time": "yyyy-mm-ddThh:mm:ssZ",
        "region": "us-east-1",
        "resources": [
            "arn:aws:ec2::us-west-2:snapshot/snap-01234567"
        ],
        "detail": {
            "event": "createSnapshot",
            "result": "succeeded",
            "cause": "",
            "request-id": "",
            "snapshot_id": "arn:aws:ec2::us-west-2:snapshot/snap-01234567",
            "source": "arn:aws:ec2::us-west-2:volume/vol-01234567",
            "StartTime": "yyyy-mm-ddThh:mm:ssZ",
            "EndTime": "yyyy-mm-ddThh:mm:ssZ"
        }
    }
    lambda_handler(event, {})
