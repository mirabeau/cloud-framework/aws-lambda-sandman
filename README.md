AWS Lambda SandMan
==================
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create lambda function with CloudFormation.
Python function will be packaged based on listed files and dependencies.
The lambda function will be uploaded to S3 using the S3 bucket provided.

For this lambda function, a log group will also be created so we can control the log expiration date. (otherwise AWS will create it for you with the 'never expires' setting once the lambda is first executed). By default this expiration is set to 7 days, but you can override this.

This role will create a CloudFormation Stacks with the following resources:
* Cloudwatch Log Group
* Lambda IAM Role
* Lambda IAM Policy
* Lambda Function
* Lambda IAM ManagedPolicy InvokePermission
* Lambda Permission
* Events Rule

![Draw.io](draw.io/services.png)

The SandMan is a function that indexes ASG, EC2 and RDS for resources tagged with (default) `mcf:scheduleDown` and `mcf:scheduleUp` tags.
An event rule will run this indexer once every hour, and once these tags have been encountered new schedule rules will be created.
One rule to bring the resource to sleep and one to restore them.
Note that this lambda assumes your schedule to be UTC, if it's not set the `mcf:scheduleTimeZone` to whatever zone you're in. (i.e. "Europe/Amsterdam")

The sleeping/waking is also done through this lambda so it can keep track of old ASG counts etc.

A test playbook has been supplied which rolls out the lambda function included with this role.
You can run this playbook using the following command:
```bash
ansible-playbook aws-lambda-sandman/tests/test.yml --inventory aws-lambda-sandman/tests/inventory.yml
```
This command should run outside of the role dir and requires the aws-utils and aws-lambda role to be in the same root dir as well.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  
Pip 18.x or higher (Python 2.7)

Required python modules:
* boto
* boto3
* awscli
* docker

Dependencies
------------
- aws-utils

Role Variables
--------------
### _Internal_
```yaml
lambda_role_path   : "{{ role_path }}"
lambda_stack_prefix: "lambda"
```
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```
Role Defaults
--------------
```yaml
---
create_changeset     : True
debug                : False
cloudformation_tags  : {}
tag_prefix           : "mcf"
lambda_log_expiration: 7
lambda_bucket_name   : "{{ account_name }}-{{ aws_region }}-lambda"

aws_lambda_ecsmonitor_params:
  create_changeset  : "{{ create_changeset }}"
  debug             : "{{ debug }}"
  environment_abbr  : "{{ account_abbr }}"
  lambda_bucket_name: "{{ lambda_bucket_name }}"
  log_expiration    : "{{ lambda_log_expiration }}"
  lambda:
    name: SandMan
    files:
      - "{{ lambda_role_path }}/files/lambda.py"
```
Example Playbooks
-----------------
Rollout the aws-lambda-sandman files with defaults
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "a-company"
    account_name    : "a-com-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"

    aws_lambda_params:
      create_s3         : True
      create_changeset  : False
      debug             : True
      environment_abbr  : "{{ account_abbr }}"
      lambda_bucket_name: "{{ account_name }}-{{ aws_region }}-lambda"
      lambda_python     : []
      lambda_roles:
        - aws-lambda-sandman

  roles:
    - aws-lambda
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
